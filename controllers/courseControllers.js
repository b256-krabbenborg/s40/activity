const Course = require ("../models/Course.js");
const auth = require("../auth");

// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
module.exports.addCourse = (course) => {

	let newCourse = new Course ({
		name: course.name,
		description: course.description,
		price: course.price
	});

	return newCourse.save().then((result,err) => {

		if(err) {

			return false;

		} else {

			return true;
		};

	});
};

// Retrieving all courses

/*
	Business Logic:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {

		return result;
	})
}

// Retrieving All Active Courses

/*
	Business Logic:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/

module.exports.getActiveCourses = () => {

	return Course.find({isActive: true}).then(result => {

		return result;
	})
}

// Retrieving a Specific Course

/*
	Business Logic:
	1. Retrieve the course that matches the course ID provided from the URL
*/

// reqParams = req.params(url) = "/:courseid"

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})

}

// Updating a Course

/*
	Business Logic:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

// course = data.course = req.body
// paramsID = data.params = req.params = /:courseId

module.exports.updateCourse = (course, paramsId) => {

	// specify the field/properties that needs to be updated

	let updatedCourse = {
		name : course.name,
		description : course.description,
		price : course.price
	}

	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result,err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}

// ACTIVITY

// Archiving a Course
/*
  Business Logic:
  1. Find the course using the course ID retrieved from the request params property
  2. Set the course's "isActive" property to false
  3. Save the updated course
*/

module.exports.archiveCourse = paramsId => {
  return Course.findById(paramsId.courseId)
    .then(course => {
      course.isActive = false;
      return course.save();
    })
    .then((result, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    });
};

