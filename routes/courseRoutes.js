const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth");

// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

  // Method #1
  // contains the data needed for creating a course
  const data = {
    course: req.body, //name, description, price
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin) {

    courseController.addCourse(data.course).then(resultFromController => res.send (resultFromController));

  } else {

    res.send(false);

  }

});

// Route for retrieving all courses

router.get("/all", (req, res) => {

  courseController.getAllCourses().then(resultFromController => res.send (resultFromController));

})

// Route for retrievimg all active courses

router.get("/active", (req, res) => {
  courseController.getActiveCourses().then(resultFromController => res.send (resultFromController));
})

// Route for retrieving a specific course

router.get("/:courseId", (req, res) => {
  courseController.getCourse(req.params).then(resultFromController => res.send (resultFromController));
})


// Route for updating a course

router.put("/update/:courseId", auth.verify, (req,res) => {
  const data ={
    course : req.body,
    isAdmin : auth.decode(req.headers.authorization).isAdmin,
    params: req.params
  }

  if(data.isAdmin) {
    courseController.updateCourse(data.course, data.params).then(resultFromController => res.send (resultFromController));
  } else {
    res.send (false);
  }
})

// ACTIVITY

// Route for archiving a course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    params: req.params
  };

  if (data.isAdmin) {
    courseController
      .archiveCourse(data.params)
      .then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }
});


module.exports = router;
